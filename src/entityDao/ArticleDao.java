package entityDao;

import globalVariable.GlobalVariable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import database.*;

public class ArticleDao {
	public static ArrayList<String> getArrayListColumnContent(Integer id,
			String columnName) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		ArrayList<String> arrayList = new ArrayList<>();

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT " + columnName + " FROM "
					+ GlobalVariable.tableName + " where id = " + id;
			rs = sta.executeQuery(sql);

			String str4 = rs.getString(columnName);

			final String[] splittedStr4 = str4.split(","); // 將字串str4以逗號分割,其結果存在splittedStr4字串陣列中
			for (String s : splittedStr4) {
				s=s.replace("[", "");
				s=s.replace("]", "");
				s=s.trim();
				arrayList.add(s);
				// showSplittedStr4 = showSplittedStr4 + "[" + s + "]";
			}
			System.out.println(arrayList);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}
		return arrayList;
	}
	public static ArrayList<List> getArrayListListColumnContent(Integer id,
			String columnName) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		ArrayList<List> arrayList = new ArrayList<List>();

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT " + columnName + " FROM "
					+ GlobalVariable.tableName + " where id = " + id;
			rs = sta.executeQuery(sql);

			String str4 = rs.getString(columnName);

			final String[] splittedStr4 = str4.split("\n"); // 將字串str4以逗號分割,其結果存在splittedStr4字串陣列中
			for (String s : splittedStr4) {
				List<String> list = new ArrayList<String>();
				list.add(s);
				arrayList.add(list);
				// showSplittedStr4 = showSplittedStr4 + "[" + s + "]";
			}
			System.out.println(arrayList);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}
		return arrayList;
	}

	public static List<Integer> getAllId() {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		List<Integer> list = new ArrayList<Integer>();
		Integer columnContent = null;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT id FROM " + GlobalVariable.tableName;
			rs = sta.executeQuery(sql);

			while (rs.next()) {
				columnContent = rs.getInt("id");
				list.add(columnContent);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}
		return list;
	}

	public static List<String> getRuleExample(String rule, String columnName) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		List<String> list = new ArrayList<String>();
		String columnContent = null;
		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			// String sql = "SELECT id FROM yahoo30kTable";
			String sql = "SELECT " + columnName + " FROM "
					+ GlobalVariable.tableName + " WHERE " + columnName
					+ " LIKE '%" + rule + "%' LIMIT 0, 1000";
			rs = sta.executeQuery(sql);

			while (rs.next()) {
				 columnContent = rs.getString(columnName).trim();
				 list.add(columnContent);
//				final String[] splittedStrWrap = columnContent.split("\n"); // 將字串以\n分割,其結果存在字串陣列中
//				for (String noWrapString : splittedStrWrap) {
//					if (noWrapString.contains(",")) {
//						final String[] splittedStrComma = noWrapString
//								.split(","); // 將字串以逗號分割,其結果存在字串陣列中
//						for (String noCommaString : splittedStrComma) {
//							if (noCommaString.contains(rule)) {
//								list.add(noCommaString.trim());
//							}
//						}
//					} else if (noWrapString.contains(rule)) {
//						list.add(noWrapString.trim());
//					}
					// if (s.contains(rule)) {
					//
					// list.add(s);
					// }

					// System.out.println(s);
					// showSplittedStr4 = showSplittedStr4 + "[" + s + "]";
				}
//			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}
		return list;
	}

	public static String getStringColumnContent(Integer id, String columnName) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		String columnContent = null;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT " + columnName + " FROM "
					+ GlobalVariable.tableName + " where id = " + id;

			rs = sta.executeQuery(sql);
			columnContent = rs.getString(columnName);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}
		return columnContent;
	}

	public static ArrayList getAllArticle() {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		ArrayList<Integer> allEventArrayList = new ArrayList<Integer>();
		ArrayList<String> allEventArrayListPicture = new ArrayList<String>();

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT * FROM " + GlobalVariable.tableName
					+ " order by id desc";

			rs = sta.executeQuery(sql);
			while (rs.next()) {
				int head = rs.getInt("id");
				String picture = rs.getString("fileName");
				allEventArrayList.add(head);
				allEventArrayListPicture.add(picture);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}

		return allEventArrayList;
	}

	public static void InsertArticle(Integer id, String article, String ckip,
			String questionPersonAllPos, String questionPersonSomePos) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			conn = dbmanage.initDB();
			// sta = (Statement) conn.createStatement();
			pst = conn
					.prepareStatement("INSERT INTO "
							+ GlobalVariable.tableName
							+ " (id,article,ckip,questionPersonAllPos,questionPersonSomePos) VALUES(?,?,?,?,?)");

			pst.setInt(1, id);
			pst.setString(2, article);
			pst.setString(3, ckip);
			pst.setString(4, questionPersonAllPos);
			pst.setString(5, questionPersonSomePos);

			pst.addBatch();
			pst.executeBatch();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDBBatch(pst, conn);
		}
	}

	public static void updateStringColumnContent(Integer id, String columnName,
			String columnContent) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		try {
			conn = dbmanage.initDB();
			sta = (Statement) conn.createStatement();
			String sql = "UPDATE " + GlobalVariable.tableName + " SET "
					+ columnName + " =  " + "'" + columnContent + "'"
					+ " where id = " + id;
			sta.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(sta, conn);
		}
	}

	public static void updateArrayListColumnContent(Integer id,
			String columnName, ArrayList<List> columnContent) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		try {
			conn = dbmanage.initDB();
			sta = (Statement) conn.createStatement();
			String sql = "UPDATE " + GlobalVariable.tableName + " SET "
					+ columnName + " =  " + "'" + columnContent + "'"
					+ " where id = " + id;
			sta.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(sta, conn);
		}
	}
	public static void updateArrayColumnContent(Integer id,
			String columnName, ArrayList<String> columnContent) {
		DbMgr dbmanage = new DbMgr();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;

		try {
			conn = dbmanage.initDB();
			sta = (Statement) conn.createStatement();
			String sql = "UPDATE " + GlobalVariable.tableName + " SET "
					+ columnName + " =  " + "'" + columnContent + "'"
					+ " where id = " + id;
			sta.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(sta, conn);
		}
	}
}
