package ner_sqlite;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import database.WriteListToExcelFile;
import builder.Director;
import builder.NerBuilder;
import builder.PersonBuilder;
import builder.PersonSomeBuilder;
import entity.Article;
import entityDao.ArticleDao;

public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
//		readSampleAndWrite();

		 Controller controller = new Controller();
		 controller.nameChain();
	}

	public static void readSampleAndWrite() {
		try {
			// ArrayList<List> randomArrayList = new ArrayList<List>();
			// sampling("Nc+Na+Nb &&", randomArrayList); // sampling
			// WriteListToExcelFile.writeArrayListList("1234.xlsx",
			// randomArrayList, "Nc+Na+Nb &&");// wrtie in excel
			for (String ruleName : readRule()) {
				// System.out.println(ruleName);
				ArrayList<List> randomArrayList = new ArrayList<List>();
				sampling(ruleName, randomArrayList); // sampling
				WriteListToExcelFile.writeArrayListList(ruleName+".xlsx",
						randomArrayList, ruleName);// wrtie in excel
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static List<String> readRule() throws IOException {
		// TODO Auto-generated method stub
		List<String> ruleNameList = new ArrayList<String>();

		try {

			FileInputStream fstream;

			fstream = new FileInputStream("personDaDaRule.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fstream));

			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				// System.out.println(strLine);
				strLine = strLine.replace("(", "");
				strLine = strLine.replace(")", "");

				ruleNameList.add(strLine + " &&");
			}

			// Close the input stream
			br.close();
			return ruleNameList;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ruleNameList;

	}

	public static ArrayList<List> sampling(String ruleName,
			ArrayList<List> randomArrayList) {
		// String column = "questionPersonAllPosSomeRule";
		// System.out.println(list.get(3));

		// for (String s : list) {
		// System.out.println(s);
		// }
		try {
			String column = "jsonAllPos";

			List<String> list = ArticleDao.getRuleExample(ruleName, column);
			List<String> newList = new ArrayList<>();
			for (String json : list) {

				JSONObject jsonObject = new JSONObject(json);

				JSONArray array = jsonObject.getJSONArray("key");
				for (int n = 0; n < array.length(); n++) {
					JSONObject object = array.getJSONObject(n);
					if (object.get("example").toString().contains(ruleName)) {
						// System.out.println(object.get("name").toString());
						newList.add(object.get("name").toString()
								+ object.get("example").toString());
					}
					// System.out.println(
					// "apple: "+object.get("name").toString());
					// System.out.println( object.get("example").toString());

					// System.out.println("object: "+object);
					// do some stuff....
				}
			}
			// System.out.println("list "+newList);
			for (int i = 1; i < 6; i++) {
				List<String> randomList = new ArrayList<String>();

				// System.out.println("--------------");
				for (int j = 1; j < 11; j++) {
					String a = newList.get((int) (Math.random() * newList
							.size()));
					newList.remove(a);
					randomList.add(a);
				}
				randomArrayList.add(randomList);

				// System.out.println(randomList);

			}
			return randomArrayList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return randomArrayList;

	}
}







