package ner_sqlite;

import builder.Director;
import builder.NerBuilder;
import builder.PersonBuilder;
import builder.PersonSomeBuilder;
import entity.Answer;
import entity.Article;
import entityDao.ArticleDao;

public class Controller {
	public void nameChain() throws CloneNotSupportedException {
		Director waiter = new Director();
		NerBuilder personBuilder = new PersonBuilder();
		NerBuilder personSomeBuilder = new PersonSomeBuilder();

		waiter.setNerBuilder(personBuilder);
//		for (int id : ArticleDao.getAllId()) {
//			System.out.println(id);
//			waiter.constructArticle(id);
//		}
//		for(int id = 1; id<10;id++){  //31942
//			System.out.println(id);
//			waiter.constructArticle(id);
//		}
//		 waiter.constructArticle(326);
		 waiter.constructArticle(1);

//		 Article article = waiter.getArticle();
	}
	public void compareRule() {
		Director waiter = new Director();
		NerBuilder personBuilder = new PersonBuilder();
		NerBuilder personSomeBuilder = new PersonSomeBuilder();

		waiter.setNerBuilder(personSomeBuilder);
//		for (int id : ArticleDao.getAllId()) {
//			System.out.println(id);
//			waiter.constructArticle(id);
//		}
		// waiter.constructArticle(27077);
		// Article article = waiter.getArticle();
	}

	public void compareAnswer(String answerDirectory, String questionDirectory,
			String missAndWrongDirectory, String resultDirectory)
			throws Exception {
		Answer answer = new Answer();
		answer.mainCompareAll(answerDirectory, questionDirectory);
		// view.saveMissandWrong(answer.getMissAndWrong(), resultDirectory + "/"
		// + "missAndWrong.txt");
		// WriteListToExcelFile.writeAnswerTermToFile("MET2_result/test.xls",
		// answer.getTermResultArray());
	}
}
