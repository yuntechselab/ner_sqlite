package rule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import database.FileTools;
import entity.Article;


public abstract class Rule {
	ArrayList<List> ruleArray = new ArrayList<List>();

	public void readRule(String ruleName) throws IOException {
		// TODO Auto-generated method stub
		String file = FileTools.readFile(ruleName);
		String splitHeadToken = "+"; 
		int ruleCount = 0;
		for (String s : file.split("\n")) {
			String[] rule = s.split("[+]");
			List<String> ruleList = new ArrayList<String>();
			for (int order = 0; order < rule.length; order++) {
				ruleList.add(rule[order].trim());
				// System.out.println(rule[order]);
			}
			ruleArray.add(ruleList);
			ruleCount++;
		}
	}



	public abstract ArrayList<List> nameEntityRecognize(Article article) throws IOException;

}
