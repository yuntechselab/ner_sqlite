package rule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import entity.Article;


public class OrgRule extends Rule{
	public OrgRule(){
		try {
			readRule("placeRule.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ArrayList<List> nameEntityRecognize(Article article)
			throws IOException {
		// TODO Auto-generated method stub
		ArrayList<List> compareRuleResult = new ArrayList<List>();
		System.out.println("Rule count: " + ruleArray.size());
		

		for (int pos = 0; pos < article.getPosList().size(); pos++) {
			for (int rule = 0; rule < ruleArray.size(); rule++) {
				String ruleanswer = "";
				String posanswer = "";
				List<String> ruleList = ruleArray.get(rule);
				// System.out.println("Round: "+pos+" ruleList: "+ ruleList);
				int order = 0;

				if (article.getPosList().get(pos).equals(ruleList.get(order))) {
					ruleanswer += ruleList.get(order);
					posanswer += article.getTermList().get(pos);
					// System.out.println("round"+ rule +"answer"+ruleanswer);
					int fitPosition = pos + 1;
					for (; fitPosition < article.getPosList().size(); fitPosition++) {
						order++;
						if (order < ruleList.size()) {
							if (article.getPosList().get(fitPosition).equals(
									ruleList.get(order))) {
								ruleanswer += "+";
								ruleanswer += ruleList.get(order);
								posanswer += article.getTermList().get(fitPosition);
							} else {
								// System.out.println(answer);
								order = 0;
								break;
							}
						} else {
							System.out.println(ruleanswer + " && " + posanswer);
							order = 0;
							// insert Result
							List<String> result = new ArrayList<String>();
							result.add(ruleanswer + " && " + posanswer);
							compareRuleResult.add(result);
							 System.out.println("Answer: "+ ruleanswer + posanswer);
							ruleanswer = "";
							posanswer = "";
							break;
						}
					}
				} else {
					ruleanswer = "";
					posanswer = "";
				}
			}
			// System.out.println("new round");
		}
		return compareRuleResult;
	}
}
