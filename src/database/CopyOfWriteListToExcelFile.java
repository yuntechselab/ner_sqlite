package database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import entity.AnswerTerm;

public class CopyOfWriteListToExcelFile {
	@SuppressWarnings("unchecked")
	public static void writeArrayListList(String fileName,
			@SuppressWarnings("rawtypes") ArrayList<List> randomArrayList,
			String ruleName) throws Exception {
		File file = new File(fileName);
		Workbook workbook = null;
		if (file.exists()) {
			InputStream inp = new FileInputStream(fileName);
			 workbook = WorkbookFactory.create(inp);
		}
//		Workbook workbook = getWorkbook(fileName);

		// Read the spreadsheet that needs to be updated
		// FileInputStream input_document = new FileInputStream(fileName);
		// Access the workbook
		// XSSFWorkbook my_xls_workbook = new XSSFWorkbook(input_document);
		// Access the worksheet, so that we can update / modify it.
		// XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
//		Sheet sheet =  workbook.getSheetAt(0);
		Sheet sheet = workbook.createSheet(ruleName);

		for (int i = 0; i < 30; i++) {
			sheet.createRow(i);
			for (int j = 0; j < 30; j++) {
				sheet.getRow(i).createCell(j);
			}
		}
		// sheet.createRow(0).createCell(0).setCellValue("header");
		// sheet.getRow(0).createCell(1).setCellValue("欄位[1]標題");
		// sheet.getRow(0).createCell(2).setCellValue("欄位[2]標題");
		// sheet.getRow(0).createCell(3).setCellValue("欄位[3]標題");
		// sheet.getRow(0).createCell(4).setCellValue("欄位[4]標題");
		// sheet.getRow(0).createCell(5).setCellValue("欄位[5]標題");
		// sheet.getRow(0).createCell(6).setCellValue("欄位[6]標題");
		// sheet.getRow(0).createCell(7).setCellValue("欄位[7]標題");

		Iterator<List> arrayListIterator = randomArrayList.iterator();

		int rowIndex = 0;
		while (arrayListIterator.hasNext()) {
			List<String> arrayListResult = arrayListIterator.next();
			Iterator<String> listIterator = arrayListResult.iterator();
			int colIndex = 0;
//			System.out.println("--------");

			while (listIterator.hasNext()) {
				String listResult = listIterator.next();
				sheet.getRow(colIndex).getCell(rowIndex)
						.setCellValue(listResult);
				// System.out.println(rowIndex + " " + colIndex);
//				System.out.println(listResult);
				colIndex++;
			}
			rowIndex++;
		}
//		saveFile(fileName, workbook);
		// lets write the excel data to file now
		 FileOutputStream fos = new FileOutputStream(fileName);
		 workbook.write(fos);
		 fos.close();
		 System.out.println(fileName + " written successfully");
	}

	public static void saveFile(String fileName, Workbook workbook)
			throws IOException {
		try {
			// FileInputStream file = new FileInputStream(fileName);
			File file = new File(fileName);
			if (file.exists()) {
				System.out.println("Appending to existing workbook '" + file
						+ "'");
				final InputStream is = new FileInputStream(file);
				try {
					workbook = new XSSFWorkbook(is);
				} finally {
					is.close();
				}
			} else {
				FileOutputStream fos = new FileOutputStream(fileName);
				try {
					workbook.write(fos);
					fos.close();
					System.out.println("no file exist");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(fileName + " written successfully");

		// HSSFWorkbook workbook = null;
		// File file = new File(context.getExternalFilesDir(null),
		// "Sample.xls");
		// FileOutputStream fileOut = new FileOutputStream(file);
		//
		// if (file.exists()) {
		// try {
		// workbook = (HSSFWorkbook)WorkbookFactory.create(file);
		// } catch (InvalidFormatException e) {
		// e.printStackTrace();
		// }
		// HSSFSheet sheet = workbook.createSheet("Sample sheet2");
		// }
		// else{
		// workbook = new HSSFWorkbook();
		// HSSFSheet sheet = workbook.createSheet("Sample sheet1");
		// }
		// workbook.write(fileOut);
		// fileOut.close();
	}

	public static Workbook getWorkbook(String fileName) throws Exception {
		if (fileName.endsWith("xlsx")) {
			return new XSSFWorkbook();
		} else if (fileName.endsWith("xls")) {
			return new HSSFWorkbook();
		} else {
			throw new Exception("invalid file name, should be xls or xlsx");
		}
	}

	public static void writeCountryListToFile(String fileName,
			List<String> countryList) throws Exception {
		Workbook workbook = null;

		if (fileName.endsWith("xlsx")) {
			workbook = new XSSFWorkbook();
		} else if (fileName.endsWith("xls")) {
			workbook = new HSSFWorkbook();
		} else {
			throw new Exception("invalid file name, should be xls or xlsx");
		}

		Sheet sheet = workbook.createSheet("Countries");
		Iterator<String> iterator = countryList.iterator();

		int rowIndex = 0;
		while (iterator.hasNext()) {
			String country = iterator.next();
			Row row = sheet.createRow(rowIndex++);
			Cell cell0 = row.createCell(0);
			cell0.setCellValue(country);
			Cell cell1 = row.createCell(1);
			cell1.setCellValue("TEXT");
			// Cell cell0 = row.createCell(0);
			// cell0.setCellValue(country.getName());
			// Cell cell1 = row.createCell(1);
			// cell1.setCellValue(country.getShortCode());
		}

		// lets write the excel data to file now
		FileOutputStream fos = new FileOutputStream(fileName);
		workbook.write(fos);
		fos.close();
		System.out.println(fileName + " written successfully");
	}

	public static void writeCountryListToFile(String fileName,
			ArrayList<List> calculateResultArray) throws Exception {
		// TODO Auto-generated method stub
		Workbook workbook = null;

		if (fileName.endsWith("xlsx")) {
			workbook = new XSSFWorkbook();
		} else if (fileName.endsWith("xls")) {
			workbook = new HSSFWorkbook();
		} else {
			throw new Exception("invalid file name, should be xls or xlsx");
		}

		Sheet sheet = workbook.createSheet("Test");

		Iterator<List> iterator = calculateResultArray.iterator();

		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Article Number");
		header.createCell(0).setCellValue("Answer Number");
		header.createCell(1).setCellValue("Wrong Number");
		header.createCell(2).setCellValue("Correct Number");
		header.createCell(3).setCellValue("Lost Number");
		header.createCell(4).setCellValue("Precision");
		header.createCell(5).setCellValue("Recall");

		int rowIndex = 1;
		while (iterator.hasNext()) {
			List list = iterator.next();
			Row row = sheet.createRow(rowIndex++);
			for (int i = 0; i < list.size(); i++) {
				row.createCell(i).setCellValue((double) list.get(i));
				// Cell cell0 = row.createCell(0);
				// cell0.setCellValue((double) list.get(0));

			}

		}

		// Row dataRow = sheet.createRow(1);
		// dataRow.createCell(0).setCellValue(14500d);
		// dataRow.createCell(1).setCellValue(9.25);
		// dataRow.createCell(2).setCellValue(3d);
		// dataRow.createCell(3).setCellFormula("A2*B2*C2");
		// dataRow.createCell(4).setCellFormula("A2*B2*C2");
		// lets write the excel data to file now
		FileOutputStream fos = new FileOutputStream(fileName);
		workbook.write(fos);
		fos.close();
		System.out.println(fileName + " written successfully");
	}

	public static void writeAnswerTermToFile(String fileName,
			ArrayList<AnswerTerm> termResultArray) throws Exception {
		// TODO Auto-generated method stub
		Workbook workbook = null;

		if (fileName.endsWith("xlsx")) {
			workbook = new XSSFWorkbook();
		} else if (fileName.endsWith("xls")) {
			workbook = new HSSFWorkbook();
		} else {
			throw new Exception("invalid file name, should be xls or xlsx");
		}

		Sheet sheet = workbook.createSheet("Test");

		Iterator<AnswerTerm> iterator = termResultArray.iterator();

		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Article Name");
		header.createCell(1).setCellValue("Answer Number");
		header.createCell(2).setCellValue("Wrong Number");
		header.createCell(3).setCellValue("Correct Number");
		header.createCell(4).setCellValue("Lost Number");
		header.createCell(5).setCellValue("Precision");
		header.createCell(6).setCellValue("Recall");

		int rowIndex = 1;
		while (iterator.hasNext()) {
			AnswerTerm answerTerm = iterator.next();
			Row row = sheet.createRow(rowIndex++);
			row.createCell(0).setCellValue(answerTerm.getFileName());
			row.createCell(1).setCellValue(answerTerm.getAnswerNumber());
			row.createCell(2).setCellValue(answerTerm.getWrongNumber());
			row.createCell(3).setCellValue(answerTerm.getCorrectNumber());
			row.createCell(4).setCellValue(answerTerm.getLostNumber());
			row.createCell(5).setCellValue(answerTerm.getPrecision());
			row.createCell(6).setCellValue(answerTerm.getRecall());
		}

		// lets write the excel data to file now
		FileOutputStream fos = new FileOutputStream(fileName);
		workbook.write(fos);
		fos.close();
		System.out.println(fileName + " written successfully");
	}

	// public static void main(String args[]) throws Exception{
	// List<String> list = ReadExcelFileToList.readExcelData("D://test.xls");
	// WriteListToExcelFile.writeCountryListToFile("D://test1.xls", list);
	// }
}
