package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;

public class DbMgr {
	  public Connection initDB() {
	        Connection conn = null;

	        try {
	        	
	        	SQLiteConfig config = new SQLiteConfig();
	    		// config.setReadOnly(true);
	    		config.setSharedCache(true);
	    		config.enableRecursiveTriggers(true);

	    		SQLiteDataSource ds = new SQLiteDataSource(config);
	    		ds.setUrl("jdbc:sqlite:sample.db");
	    		conn =  ds.getConnection();
	    		return ds.getConnection();
	    		// ds.setServerName("sample.db");
	        	
//	            Class.forName("com.mysql.jdbc.Driver");
//	            String url = "jdbc:mysql://140.125.84.46:3306/supermen_db";
//	            String url = "jdbc:mysql://140.125.84.46:3306/supermen_db?useUnicode=true&characterEncoding=UTF-8";

//	            conn = DriverManager.getConnection(url, "root", "1234");
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }

	        return conn;
	    }

	    public void closeDB(Statement sta, Connection conn) {
	        try {

	            sta.close();
	            conn.close();
	        } catch (SQLException e) {
	            e.printStackTrace();  
	        }
	    }

	    public void closeDB(ResultSet rs, Statement sta, Connection conn) {
	        try {
	            rs.close();
	            sta.close();
	            conn.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }

	    public void closeDBBatch( PreparedStatement pst, Connection conn) {
	        try {

	            pst.close();
	            conn.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
}
