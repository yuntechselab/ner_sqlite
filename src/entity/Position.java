package entity;

import java.util.ArrayList;

public class Position {
	private int position;
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public ArrayList<String> getRuleExample() {
		return ruleExample;
	}
	public void setRuleExample(ArrayList<String> ruleExample) {
		this.ruleExample = ruleExample;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private ArrayList<String> ruleExample = new ArrayList<>();;
	private String name;
}
