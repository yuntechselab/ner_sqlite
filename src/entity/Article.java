package entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

public class Article {
	private HashMap<Integer,String> namePositionMap = new HashMap<>();
	private HashMap<Integer,ArrayList<String>> namePositionRuleMap = new HashMap<>();
	public HashMap<Integer, ArrayList<String>> getNamePositionRuleMap() {
		return namePositionRuleMap;
	}
	public void setNamePositionRuleMap(
			HashMap<Integer, ArrayList<String>> namePositionRuleMap) {
		this.namePositionRuleMap = namePositionRuleMap;
	}
	public HashMap<Integer, String> getNamePositionMap() {
		return namePositionMap;
	}
	public void setNamePositionMap(HashMap<Integer, String> namePositionMap) {
		this.namePositionMap = namePositionMap;
	}
	public List<String> getPosList() {
		return posList;
	}
	public void setPosList(List<String> posList) {
		this.posList = posList;
	}
	public List<String> getTermList() {
		return termList;
	}
	public void setTermList(List<String> termList) {
		this.termList = termList;
	}
	public ArrayList<List> getAllPosPersonResult() {
		return allPosPersonResult;
	}
	public void setAllPosPersonResult(ArrayList<List> allPosPersonResult) {
		this.allPosPersonResult = allPosPersonResult;
	}
//	public ArrayList<String> getAllPosPerson() {
//		return allPosPerson;
//	}
//	public void setAllPosPerson(ArrayList<String> allPosPerson) {
//		this.allPosPerson = allPosPerson;
//	}
	public ArrayList<List> getSomePosPersonResult() {
		return somePosPersonResult;
	}
	public void setSomePosPersonResult(ArrayList<List> somePosPersonResult) {
		this.somePosPersonResult = somePosPersonResult;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCkipResult() {
		return ckipResult;
	}
	public void setCkipResult(String ckipResult) {
		this.ckipResult = ckipResult;
	}
	List<String> posList = new ArrayList<String>();
	List<String> termList = new ArrayList<String>();
	ArrayList<List> allPosPersonResult = new ArrayList<List>();
//	ArrayList<String> allPosPerson = new ArrayList<String>();
	ArrayList<List> somePosPersonResult = new ArrayList<List>();
	Integer id;
	String content;
	String ckipResult;
	JSONObject jsonObject = new JSONObject();
	public JSONObject getJsonObject() {
		return jsonObject;
	}
	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}
}
