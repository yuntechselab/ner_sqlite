package entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import database.FileTools;

public class Answer {
	ArrayList<List> missAndWrong = new ArrayList<List>();

	ArrayList<AnswerTerm> termResultArray = new ArrayList<AnswerTerm>();

	public ArrayList<AnswerTerm> getTermResultArray() {
		return termResultArray;
	}

	public void setTermResultArray(ArrayList<AnswerTerm> termResultArray) {
		this.termResultArray = termResultArray;
	}

	@SuppressWarnings("null")
	public void mainCompareAll(String answerDirectory, String questionDirectory) {
		// TODO Auto-generated method stub
		ArrayList<String> answerArray = new ArrayList<String>();
		ArrayList<String> questionArray = new ArrayList<String>();

		ArrayList<String> ruleArray = new ArrayList<String>();
		HashMap<String, Integer> ruleMap = new HashMap<String, Integer>();
		ruleArray = FileTools.read("personRule.txt");
		for (String rule : ruleArray) {
			ruleMap.put(rule, 0);
		}

		Directory directory = new Directory(questionDirectory);

		// 比�??
		for (String fileName : directory.getDirectoryAllFileName()) {
			// String[] a = { "14", "62", "63", "68", "82", "84", "88", "96",
						// "97" };
						// int flag = 0;
						// for (String s : a) {
						// s = s + ".txt";
						// if (s.equals(fileName)) {
						// flag = 1;
						// }
						// }
						// if (flag == 1) {
						// continue;
						// }
			
			questionArray = FileTools.read(questionDirectory + "/" + fileName);
			answerArray = FileTools.read(answerDirectory + "/" + fileName);
			compareAll(answerArray, questionArray, fileName, ruleMap, ruleArray);
		}
		// ??�總

		AnswerTerm finalAnswerTerm = new AnswerTerm();// ??終�?��??
		finalAnswerTerm.setFileName("?��?��結�??");
		for (AnswerTerm answerTerm : termResultArray) {
			finalAnswerTerm.setAnswerNumber(finalAnswerTerm.getAnswerNumber()
					+ answerTerm.getAnswerNumber());
			finalAnswerTerm.setWrongNumber(finalAnswerTerm.getWrongNumber()
					+ answerTerm.getWrongNumber());
			finalAnswerTerm.setCorrectNumber(finalAnswerTerm.getCorrectNumber()
					+ answerTerm.getCorrectNumber());
			finalAnswerTerm.setLostNumber(finalAnswerTerm.getLostNumber()
					+ answerTerm.getLostNumber());
		}

		// ??�總比�??
		System.out
				.println("answerNumber: " + finalAnswerTerm.getAnswerNumber());
		System.out.println("wrongNumber: " + finalAnswerTerm.getWrongNumber());
		System.out.println("correctNumber: "
				+ finalAnswerTerm.getCorrectNumber());
		System.out.println("lostNumber: " + finalAnswerTerm.getLostNumber());
		System.out.println("Precision: " + finalAnswerTerm.getPrecision());
		System.out.println("Recall: " + finalAnswerTerm.getRecall());

		termResultArray.add(finalAnswerTerm);

		for (Object key : ruleMap.keySet()) {
			System.out.println(key + " : " + ruleMap.get(key));
		}

	}

	public void compareAll(ArrayList<String> answerArray,
			ArrayList<String> questionArray, String fileName,
			HashMap<String, Integer> ruleMap, ArrayList<String> ruleArray) {
		HashMap<String, Integer> answerMap = new HashMap<String, Integer>();
		HashMap<String, Integer> questionMap = new HashMap<String, Integer>();

		AnswerTerm answerTerm = new AnswerTerm();
		answerTerm.setFileName(fileName);
		answerTerm.setAnswerNumber(answerArray.size());

		for (String a : answerArray) {
			answerMap.put(a, 0);
		}
		for (String a : questionArray) {
			questionMap.put(a, 0);
		}
		for (String question : questionArray) {
			for (String answer : answerArray) {
				// for (char alphabet : answer.toCharArray()) {
				// int index = question.indexOf(alphabet);
				// if (index != -1) {
				// int questionMapValue = questionMap.get(question) + 1;
				// questionMap.put(question, questionMapValue);
				// int answerMapValue = answerMap.get(answer) + 1;
				// answerMap.put(answer, answerMapValue);
				//
				// // 計�?��?��?�符??��?��??
				// for (String rule : ruleArray) {
				// int ruleIndex = question.indexOf(rule);
				// if (ruleIndex != -1) {
				// int ruleMapValue = ruleMap.get(rule) + 1;
				// ruleMap.put(rule, ruleMapValue);
				// }
				// }
				// // System.out.println("fit: " + question);
				// // System.out.println("fit: " + answer);
				// } else {
				// // System.out.println("unfit: " + question);
				// // System.out.println("unfit: " + answer);
				// }
				// }
				int index = question.indexOf(answer);
				if (index != -1) {
					int questionMapValue = questionMap.get(question) + 1;
					questionMap.put(question, questionMapValue);
					int answerMapValue = answerMap.get(answer) + 1;
					answerMap.put(answer, answerMapValue);

					// 計�?��?��?�符??��?��??
					for (String rule : ruleArray) {
						int ruleIndex = question.indexOf(rule);
						if (ruleIndex != -1) {
							int ruleMapValue = ruleMap.get(rule) + 1;
							ruleMap.put(rule, ruleMapValue);
						}
					}
					// System.out.println("fit: " + question);
					// System.out.println("fit: " + answer);
				} else {
					// System.out.println("unfit: " + question);
					// System.out.println("unfit: " + answer);
				}
			}
		}
		for (Entry<String, Integer> entry : answerMap.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if (entry.getValue() == 0) {
				// System.out.println("Miss: " + key);
				answerTerm.setLostNumber(answerTerm.getLostNumber() + 1);
				List<String> list = new ArrayList<String>();
				list.add("Miss: " + key);
				missAndWrong.add(list);
			}
		}
		for (Entry<String, Integer> entry : questionMap.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if (entry.getValue() == 0) {
				// System.out.println("Wrong Rule: " + key);
				answerTerm.setWrongNumber(answerTerm.getWrongNumber() + 1);
				List<String> list = new ArrayList<String>();
				list.add("Wrong Rule: " + key);
				missAndWrong.add(list);
			}
		}
		answerTerm.setCorrectNumber(answerTerm.getCorrectNumber());
		answerTerm.setPrecision(answerTerm.getPrecision());
		answerTerm.setRecall(answerTerm.getRecall());

		termResultArray.add(answerTerm);
	}

	public ArrayList<List> getMissAndWrong() {
		return missAndWrong;
	}

	public void setMissAndWrong(ArrayList<List> missAndWrong) {
		this.missAndWrong = missAndWrong;
	}

}
