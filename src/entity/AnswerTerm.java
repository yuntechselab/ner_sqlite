package entity;

public class AnswerTerm {
	String fileName = null;
	int answerNumber = 0;
	int wrongNumber = 0;
	int lostNumber = 0;
	int correctNumber = 0;
	double precision = 0;
	double recall = 0;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getAnswerNumber() {
		return answerNumber;
	}

	public void setAnswerNumber(int answerNumber) {
		this.answerNumber = answerNumber;
	}

	public int getWrongNumber() {
		return wrongNumber;
	}

	public void setWrongNumber(int wrongNumber) {
		this.wrongNumber = wrongNumber;
	}

	public int getCorrectNumber() {
		return answerNumber - lostNumber;
	}

	public void setCorrectNumber(int correctNumber) {
		this.correctNumber = correctNumber;
	}

	public int getLostNumber() {
		return lostNumber;
	}

	public void setLostNumber(int lostNumber) {
		this.lostNumber = lostNumber;
	}

	public double getPrecision() {
		return ((double) correctNumber / (wrongNumber + correctNumber));
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public double getRecall() {
		return ((double) correctNumber / (lostNumber + correctNumber));
	}

	public void setRecall(double recall) {
		this.recall = recall;
	}

	public AnswerTerm() {
		// TODO Auto-generated constructor stub
	}

}
