package entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RuleEntity implements Cloneable {
	private String ruleName;
	private Set<Integer> nameInRulePosition = new HashSet<Integer>();
	private int ruleLength;
	List<String> ruleNamePosList = new ArrayList<String>();
	List<String> ruleNameTermList = new ArrayList<String>();

	public RuleEntity clone() throws CloneNotSupportedException {
		return (RuleEntity) super.clone();
	}

	public List<String> getRuleNameList() {
		return ruleNamePosList;
	}

	public void setRuleNameList(List<String> ruleNameList) {
		this.ruleNamePosList = ruleNameList;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public Set<Integer> getNameInRulePosition() {
		return nameInRulePosition;
	}

	public void setNameInRulePosition(Set<Integer> nameInRulePosition) {
		this.nameInRulePosition = nameInRulePosition;
	}

	public int getRuleLength() {
		return ruleLength;
	}

	public void setRuleLength(int ruleLength) {
		this.ruleLength = ruleLength;
	}

	public List<String> getRuleNamePosList() {
		return ruleNamePosList;
	}

	public void setRuleNamePosList(List<String> ruleNamePosList) {
		this.ruleNamePosList = ruleNamePosList;
	}

	public List<String> getRuleNameTermList() {
		return ruleNameTermList;
	}

	public void setRuleNameTermList(List<String> ruleNameTermList) {
		this.ruleNameTermList = ruleNameTermList;
	}
}
