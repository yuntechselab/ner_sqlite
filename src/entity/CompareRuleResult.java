package entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CompareRuleResult {
	private List<RuleEntity> ruleEntityList = new ArrayList<RuleEntity>();
	private List<String> rule = new ArrayList<>();
	private Set<Integer> nameInRulePosition = new HashSet<Integer>();
	private List<String> realNameList = new ArrayList<>();
	public List<String> getRealNameList() {
		return realNameList;
	}

	public void setRealNameList(List<String> realNameList) {
		this.realNameList = realNameList;
	}

	public Set<Integer> getNameInRulePosition() {
		return nameInRulePosition;
	}

	public void setNameInRulePosition(Set<Integer> nameInRulePosition) {
		this.nameInRulePosition = nameInRulePosition;
	}

	private List<Integer> namePositionInArticleList = new ArrayList<>();
	public List<Integer> getNamePositionInArticleList() {
		return namePositionInArticleList;
	}

	public void setNamePositionInArticleList(List<Integer> namePositionInArticleList) {
		this.namePositionInArticleList = namePositionInArticleList;
	}

	public List<String> getRule() {
		return rule;
	}

	public void setRule(List<String> rule) {
		this.rule = rule;
	}

	public List<String> getRuleExample() {
		return ruleExample;
	}

	public void setRuleExample(List<String> ruleExanple) {
		this.ruleExample = ruleExanple;
	}

	private List<String> ruleExample = new ArrayList<>();

	private String namePosCandidate;
	private String nameTermCandidate;

	public List<RuleEntity> getRuleEntityList() {
		return ruleEntityList;
	}

	public void setRuleEntityList(List<RuleEntity> ruleEntityList) {
		this.ruleEntityList = ruleEntityList;
	}

	public String getNamePosCandidate() {
		return namePosCandidate;
	}

	public void setNamePosCandidate(String namePosCandidate) {
		this.namePosCandidate = namePosCandidate;
	}

	public String getNameTermCandidate() {
		return nameTermCandidate;
	}

	public void setNameTermCandidate(String nameTermCandidate) {
		this.nameTermCandidate = nameTermCandidate;
	}

}
