package newRule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import database.FileTools;
import entity.Article;
import entity.CompareRuleResult;
import entity.Position;
import entity.RuleEntity;

public class PersonDaDaRule extends Rule {
	ArrayList<CompareRuleResult> compareRuleResultArray = new ArrayList<>();
	ArrayList<List> realNameArrayList = new ArrayList<>();

	public PersonDaDaRule() {
		try {
			readRule("personDaDaRule.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<List> nameEntityRecognize(Article article)
			throws IOException, CloneNotSupportedException {
		// TODO Auto-generated method stub
		// System.out.println("Rule count: " + ruleArray.size());
		JSONArray array = article.getJsonObject().getJSONArray(article.getId().toString());
		for (int n = 0; n < array.length(); n++) {
			JSONObject object = array.getJSONObject(n);
//			 System.out.println("object: "+object);
		}
		
		
		
		
		ArrayList<RuleEntity> ruleArrayClone = new ArrayList<>(ruleArray);
		ArrayList<List> sentenceArrayListList = new ArrayList<List>();
		int thisTermPosition = 0;
		int lastTermPosition = 0;
		for (String term : article.getTermList()) {
			List<String> list = new ArrayList<String>();
			if (term.equals("�C") || term.equals("�A")) {
				for (; lastTermPosition < thisTermPosition; lastTermPosition++) {
					list.add(article.getTermList().get(lastTermPosition));				
				}
				sentenceArrayListList.add(list);
				lastTermPosition = thisTermPosition;
			}
			thisTermPosition++;
		}
//		System.out.println("Sentences: " + sentenceArrayListList);

		for (int posOrder = 0; posOrder < article.getPosList().size(); posOrder++) {
			for (int rule = 0; rule < ruleArray.size(); rule++) {
				CompareRuleResult compareRuleResult = new CompareRuleResult();

				String ruleanswer = "";
				String posanswer = "";

				// System.out.println("Round: "+pos+" ruleList: "+ ruleList);
				int ruleOrder = 0;

				if (article
						.getPosList()
						.get(posOrder)
						.equals(ruleArrayClone.get(rule).getRuleNameList()
								.get(ruleOrder))) {
					ruleanswer += ruleArrayClone.get(rule).getRuleNameList()
							.get(ruleOrder);
					posanswer += article.getTermList().get(posOrder);

					// System.out.println("round"+ rule +"answer"+ruleanswer);
					int fitPosition = posOrder + 1;
					for (; fitPosition < article.getPosList().size(); fitPosition++) {
						ruleOrder++;
						if (ruleOrder < ruleArrayClone.get(rule)
								.getRuleNameList().size()) {
							if (article
									.getPosList()
									.get(fitPosition)
									.equals(ruleArrayClone.get(rule)
											.getRuleNameList().get(ruleOrder))) {
								ruleanswer += "+";
								ruleanswer += ruleArrayClone.get(rule)
										.getRuleNameList().get(ruleOrder);
								posanswer += "+";
								posanswer += article.getTermList().get(
										fitPosition);
								// System.out.println("- - - - - - - -");

								// setNamePositionNumber(article,
								// compareRuleResult,
								// ruleArrayClone.get(rule), fitPosition);
								// System.out.println("- - - - - - - -");
							} else if (ruleArrayClone.get(rule)
									.getRuleNameList().get(ruleOrder)
									.equals("...")) {

							} else {
								// System.out.println(answer);
								ruleOrder = 0;
								break;
							}
						} else {
//							System.out.println(ruleanswer + " && " + posanswer);
							// System.out.println("fitPosition: "+fitPosition);
							// System.out.println("fitPosition term: "+article.getTermList().get(fitPosition));

							ruleOrder = 0;
							// insert Result
							List<String> result = new ArrayList<String>();
							result.add(ruleanswer + " && " + posanswer);
							String ruleAndExample = ruleanswer + " && "
									+ posanswer;

							// System.out.println("Answer: " + ruleanswer
							// + posanswer);
							// System.out.println(ruleAndExample);
							setNamePositionNumber(article,
									ruleArrayClone.get(rule), fitPosition,
									ruleAndExample);

							ruleanswer = "";
							posanswer = "";
							break;
						}
					}
				} else {
					ruleanswer = "";
					posanswer = "";

				}
				compareRuleResultArray.add(compareRuleResult);
				// printSinglePos(compareRuleResult);
			}

			// System.out.println("new round");
		}
		// printAllPos();
		return realNameArrayList;
	}

	private void setNamePositionNumber(Article article, RuleEntity ruleEntity,
			int fitPosition, String ruleAndExample) {
		// TODO Auto-generated method stub
		for (int namePositionInRule : ruleEntity.getNameInRulePosition()) {
			int calculateNumber = fitPosition
					- ruleEntity.getRuleNameList().size() + namePositionInRule;
			Position position = new Position();

			position.setName(article.getTermList().get(calculateNumber));
			position.setPosition(calculateNumber);
			position.getRuleExample().add(ruleAndExample);

			article.getNamePositionMap().put(calculateNumber,
					article.getTermList().get(calculateNumber));
			// System.out.println(ruleAndExample);

			if (article.getNamePositionRuleMap().get(calculateNumber) != null) {
				article.getNamePositionRuleMap().get(calculateNumber)
						.add(ruleAndExample);
				// System.out.println("if");
			} else {
				ArrayList<String> positionRule = new ArrayList<>();
				positionRule.add(ruleAndExample);
				article.getNamePositionRuleMap().put(calculateNumber,
						positionRule);
				// System.out.println("else");
			}

			// System.out.println(article.getNamePositionRuleMap());
			// System.out.println("Calculate number: " + calculateNumber);
			// System.out.println("Calculate answer: "
			// + article.getTermList().get(calculateNumber));
			// System.out.println("fitPosition: " + fitPosition);
			// System.out.println("fitPositionName: "
			// + article.getTermList().get(fitPosition));
			// System.out.println("namePositionInRule: " + namePositionInRule);
			// System.out.println("size: " +
			// ruleEntity.getRuleNameList().size());
			// System.out.println("rule: " + ruleEntity.getRuleNameList());

		}

	}

	private void printAllPos() {
		// TODO Auto-generated method stub
		// System.out.println("print all");
		// try {
		for (CompareRuleResult compareRuleResult : compareRuleResultArray) {
			if (compareRuleResult.getRealNameList().size() > 0) {
				for (String name : compareRuleResult.getRealNameList()) {
					// System.out.println(compareRuleResult.getRealNameList());
					List<String> list = new ArrayList<>();
					list.add(name);
					realNameArrayList.add(list);
				}

			}
			// if (!compareRuleResult.getRealNameList().get(0).equals("")) {
			// } else {
			// System.out.println(compareRuleResult.getRealNameList());
			// }
			// System.out.println(compareRuleResult.getRuleExample());
			// if (compareRuleResult.getRuleExample() != null
			// && !compareRuleResult.getRuleExample().equals("")) {
			//
			// } else {
			// System.out.println(compareRuleResult.getRule());
			// System.out.println(compareRuleResult.getRuleExample());
			// }

		}
		// } catch (Exception e) {
		//
		// }
	}

	private void printSinglePos(CompareRuleResult compareRuleResult) {
		// TODO Auto-generated method stub
		// System.out.println("test0" +
		// ruleEntity.getRuleNameTermList());
		// if (ruleEntity.getRuleNameTermList().get(0) != null) {
		// System.out.println("Term List: "
		// + ruleEntity.getRuleNameTermList());
		// System.out.println("Rule Name: "
		// + ruleEntity.getRuleNameList());
		// }
		for (String name : compareRuleResult.getRealNameList()) {
			System.out.println(name);
		}

	}
}
