package newRule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import entity.Article;
import entity.CompareRuleResult;
import entity.RuleEntity;

public class CopyOfPersonRule extends Rule {
	ArrayList<CompareRuleResult> compareRuleResultArray = new ArrayList<CompareRuleResult>();

	public CopyOfPersonRule() {
		try {
			readRule("personRule.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<CompareRuleResult> nameEntityRecognize(Article article)
			throws IOException {
		// TODO Auto-generated method stub
		// System.out.println("Rule count: " + ruleArray.size());
		int posPosition = 0;

		for (String pos : article.getPosList()) {
			@SuppressWarnings("unchecked")
			ArrayList<RuleEntity> ruleArrayClone = (ArrayList<RuleEntity>) ruleArray
					.clone();
			CompareRuleResult compareRuleResult = new CompareRuleResult();
			// System.out.println(pos);

			for (RuleEntity ruleEntity : ruleArrayClone) {
				int ruleIndex = 0;
				// System.out.println(ruleEntity.getRuleNameList());
				if (pos.equals(ruleEntity.getRuleNameList().get(ruleIndex))) {
					ruleEntity.getRuleNameTermList().add(
							article.getTermList().get(posPosition));
//					System.out.println("rule Pos: "
//							 + ruleEntity.getRuleNameList());
//							 System.out.println("term: "
//							 + ruleEntity.getRuleNameTermList());
					int fitPosition = posPosition + 1;
					for (; fitPosition < article.getPosList().size(); fitPosition++) {
						ruleIndex = ruleIndex + 1;
						if (ruleIndex < ruleEntity.getRuleNameList().size()) {
							if (article
									.getPosList()
									.get(fitPosition)
									.equals(ruleEntity.getRuleNameList().get(
											ruleIndex))) {
								 System.out.println("rule Pos: "
								 + ruleEntity.getRuleNameList());
								 System.out.println("term: "
								 + ruleEntity.getRuleNameTermList());
								 
								ruleEntity.getRuleNameTermList().add(
										article.getTermList().get(fitPosition));
								compareRuleResult.getRuleEntityList().add(
										ruleEntity);
							} else {
								ruleIndex = 0;
								ruleEntity.getRuleNameTermList().clear();
								break;
							}
						} else {
							ruleIndex = 0;
							ruleEntity.getRuleNameTermList().clear();
							break;

						}
					}
				}else{
					ruleEntity.getRuleNameTermList().clear();
				}
			}
			printSinglePos(compareRuleResult);
			compareRuleResultArray.add(compareRuleResult);
			posPosition = posPosition + 1;
		}
		// printAllPos(compareRuleResultArray);
		return compareRuleResultArray;
	}

	private void printAllPos(ArrayList<CompareRuleResult> compareRuleResultArray) {
		// TODO Auto-generated method stub
		try {
			for (CompareRuleResult compareRuleResult : compareRuleResultArray) {
				for (RuleEntity ruleEntity : compareRuleResult
						.getRuleEntityList()) {
					// System.out.println("test0"
					// + ruleEntity.getRuleNameTermList());
					if (ruleEntity.getRuleNameTermList().get(0) != null) {
						System.out.println("test"
								+ ruleEntity.getRuleNameTermList().get(0));
					}

				}
			}
		} catch (Exception e) {

		}
	}

	private void printSinglePos(CompareRuleResult compareRuleResult) {
		// TODO Auto-generated method stub
		for (RuleEntity ruleEntity : compareRuleResult.getRuleEntityList()) {
			try {
//				 System.out.println("test0" +
//				 ruleEntity.getRuleNameTermList());
				if (ruleEntity.getRuleNameTermList().get(0) != null) {
					System.out.println("Term List: "
							+ ruleEntity.getRuleNameTermList());
					System.out.println("Rule Name: "
							+ ruleEntity.getRuleNameList());
				}
			} catch (Exception e) {
			}
		}
	}
}
