package newRule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import database.FileTools;
import entity.Article;
import entity.CompareRuleResult;
import entity.RuleEntity;


public abstract class Rule {
	ArrayList<RuleEntity> ruleArray = new ArrayList<RuleEntity>();

	public void readRule(String ruleName) throws IOException {
		// TODO Auto-generated method stub
		String file = FileTools.readFile(ruleName);
		String splitHeadToken = "+"; 
		int ruleCount = 0;
		for (String s : file.split("\n")) {
			String[] rule = s.split("[+]");
			RuleEntity ruleEntity = new RuleEntity();
			for (int order = 0; order < rule.length; order++) {
//				 System.out.println(rule[order]);
				 
				if(rule[order].trim().contains("(")||rule[order].trim().contains(")")){
					ruleEntity.getNameInRulePosition().add(order);
					rule[order] = rule[order].replace("(", "");
					rule[order] = rule[order].replace(")", "");
//					 System.out.println(rule[order]);
				}
				ruleEntity.getRuleNameList().add(rule[order].trim());

			}
			ruleEntity.setRuleLength(rule.length);
			ruleArray.add(ruleEntity);
			ruleCount++;
		}
	}



	public abstract ArrayList<List> nameEntityRecognize(Article article) throws IOException, CloneNotSupportedException;

}
