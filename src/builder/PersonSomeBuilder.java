package builder;

import java.io.IOException;

import org.sqlite.util.StringUtils;

import rule.PersonRule;
import rule.PersonSomeRule;
import rule.Rule;
import ner_sqlite.Ckip;
import entityDao.ArticleDao;

public class PersonSomeBuilder extends NerBuilder {

	@Override
	public void compareRule() {
		// TODO Auto-generated method stub
		Rule personSomeRule = new PersonSomeRule();
		String allPosPersonResult = ArticleDao.getStringColumnContent(
				article.getId(), "questionPersonAllPosSomeRule");
		try {
			// if (allPosPersonResult != null && !allPosPersonResult.isEmpty())
			// {
			//
			// } else {
			ArticleDao.updateArrayListColumnContent(article.getId(),
					"questionPersonAllPosSomeRule",
					personSomeRule.nameEntityRecognize(article));
			System.out.println(personSomeRule.nameEntityRecognize(article));
			// }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void keepPos() {
		// TODO Auto-generated method stub

	}

	@Override
	public void compareNameChain() {
		// TODO Auto-generated method stub

	}

}
