package builder;

import entity.Article;

public class Director {
	private NerBuilder nerBuilder;

	  public void setNerBuilder(NerBuilder nb) { nerBuilder = nb; }
	  public Article getArticle() { return nerBuilder.getArticle(); }

	  public void constructArticle(int id) throws CloneNotSupportedException {
		  nerBuilder.createNewArticle();
		  nerBuilder.importArticle(id);
		  //nerBuilder.ckipMethod();
		  nerBuilder.segement();
//		  nerBuilder.keepPos();
		  nerBuilder.compareRule();
		  nerBuilder.compareNameChain();
	  }
}
