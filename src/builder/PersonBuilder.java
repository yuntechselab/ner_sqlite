package builder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import database.FileTools;
import newRule.PersonDaDaRule;
import newRule.PersonRule;
import newRule.Rule;
import ner_sqlite.Ckip;
import entity.Article;
import entity.Position;
import entityDao.ArticleDao;
import globalVariable.GlobalVariable;

public class PersonBuilder extends NerBuilder {

	@Override
	public void keepPos() {
		// TODO Auto-generated method stub

	}

	public void compareNameChain() {
//		compareNameChainLessTwo();
//		compareNameChainEqualTwo();
	}

	public void compareNameChainEqualTwo() {
		// System.out.println("content: " + article.getContent());
		// System.out.println("pos: " + article.getPosList());
		// System.out.println("term: " + article.getTermList());
		// ArrayList<String> realNameArray =
		// ArticleDao.getArrayListColumnContent(article.getId(),
		// GlobalVariable.allPosPersonColumnName) ;

		// System.out.println( article.getNamePositionMap());
		int count = 0;
		String nameCandidateMother = null;
		String nameCandidateChild = null;
		double times = 0;

		for (String term : article.getTermList()) {
			for (HashMap.Entry<Integer, String> entry : article
					.getNamePositionMap().entrySet()) {
				Integer key = entry.getKey();
				if (count == key && term.length() == 2) {
					// System.out.println("Term: " + term);
					// FileTools.writeText("Id: " + article.getId() + "\n",
					// "Console.txt", "UTF-8", true);
					// FileTools.writeText("Term: " + term + "\n",
					// "Console.txt",
					// "UTF-8", true);
					// if nb's term < 2
					// System.out.println("Name: " + term + "  "
					// + article.getTermList().get(count));

					if (article.getTermList().get(count + 1).length() > 1) {
						nameCandidateMother = article.getTermList().get(count)
								+ article.getTermList().get(count + 1)
										.substring(0, 1);
						nameCandidateChild = article.getTermList().get(count)
								+ article.getTermList().get(count + 1)
										.substring(0, 2);
					} else if (article.getTermList().get(count + 1).length() == 1) {
						nameCandidateMother = article.getTermList().get(count)
								+ article.getTermList().get(count + 1);
						nameCandidateChild = article.getTermList().get(count)
								+ article.getTermList().get(count + 1)
								+ article.getTermList().get(count + 2)
										.substring(0, 1);
					}
					// ----------------
					// System.out.println("Child: " + nameCandidateChild);
					// System.out.println("Mother: " + nameCandidateMother);
					// ---------------
					double candidateChildTimes = Double
							.valueOf(StringMatchingAlgo(article.getContent(),
									nameCandidateChild));
					double candidateMotherTimes = Double
							.valueOf(StringMatchingAlgo(article.getContent(),
									nameCandidateMother));
					times = candidateChildTimes / candidateMotherTimes;
					// ----------------
					// System.out.println("childTimes(i+2): "
					// + candidateChildTimes);
					// System.out.println("motherTimes(i+1): "
					// + candidateMotherTimes);
					// System.out.println("calculatedTimes: " + times);
					// ----------------

					if (times < 1.0) {
						article.getNamePositionMap().put(key,
								nameCandidateMother);
						// System.out.println("Answer: " + nameCandidateMother);
						// FileTools.writeText("Answer: " + nameCandidateMother
						// + "\n", "Console.txt", "UTF-8", true);

					} else if (times >= 1.0) {
						if (candidateChildTimes >= 2) { // child's times >= 2
							article.getNamePositionMap().put(key,
									nameCandidateChild);
							// System.out.println("Answer: " +
							// nameCandidateChild);
							// FileTools.writeText("Answer: " +
							// nameCandidateChild
							// + "\n", "Console.txt", "UTF-8", true);
						}
					}
				}
			}
			count++;
		}
		// ----------------
		// System.out.println(article.getNamePositionMap());
		// System.out.println(article.getNamePositionRuleMap());
		// ----------------

		ArrayList<String> arrayList = new ArrayList<>();
		for (String value : article.getNamePositionMap().values()) {
			arrayList.add(value);
		}
		ArticleDao.updateArrayColumnContent(article.getId(),
				GlobalVariable.allPosPersonColumnName, arrayList);
		convertJson();

	}

	public void compareNameChainLessTwo() {
		// System.out.println("content: " + article.getContent());
		// System.out.println("pos: " + article.getPosList());
		// System.out.println("term: " + article.getTermList());
		// ArrayList<String> realNameArray =
		// ArticleDao.getArrayListColumnContent(article.getId(),
		// GlobalVariable.allPosPersonColumnName) ;
		// System.out.println( article.getNamePositionMap());
		int count = 0;
		String nameCandidateMother = null;
		String nameCandidateChild = null;
		double times = 0;

		for (String term : article.getTermList()) {
			for (HashMap.Entry<Integer, String> entry : article
					.getNamePositionMap().entrySet()) {
				Integer key = entry.getKey();
				if (count == key && term.length() < 2) {
					// System.out.println("Term: " + term);

					// if nb's term < 2
					// System.out.println("Name: " + term + "  "
					// + article.getTermList().get(count));

					if (article.getTermList().get(count + 1).length() > 1) {
						nameCandidateMother = article.getTermList().get(count)
								+ article.getTermList().get(count + 1)
										.substring(0, 1);
						nameCandidateChild = article.getTermList().get(count)
								+ article.getTermList().get(count + 1)
										.substring(0, 2);
					} else if (article.getTermList().get(count + 1).length() == 1) {
						nameCandidateMother = article.getTermList().get(count)
								+ article.getTermList().get(count + 1);
						nameCandidateChild = article.getTermList().get(count)
								+ article.getTermList().get(count + 1)
								+ article.getTermList().get(count + 2)
										.substring(0, 1);
					}
					// ----------------
					// System.out.println("Child: " + nameCandidateChild);
					// System.out.println("Mother: " + nameCandidateMother);
					// ---------------
					double candidateChildTimes = Double
							.valueOf(StringMatchingAlgo(article.getContent(),
									nameCandidateChild));
					double candidateMotherTimes = Double
							.valueOf(StringMatchingAlgo(article.getContent(),
									nameCandidateMother));
					times = candidateChildTimes / candidateMotherTimes;
					// ----------------
					// System.out.println("childTimes(i+2): "
					// + candidateChildTimes);
					// System.out.println("motherTimes(i+1): "
					// + candidateMotherTimes);
					// System.out.println("calculatedTimes: " + times);
					// ----------------

					if (times < 1.0) {
						article.getNamePositionMap().put(key,
								nameCandidateMother);
						// System.out.println("Answer: " + nameCandidateMother);
					} else if (times >= 1.0) {
						if (candidateChildTimes >= 2) { // child's times >= 2
							article.getNamePositionMap().put(key,
									nameCandidateChild);
							// System.out.println("Answer: " +
							// nameCandidateChild);
						}
					}
				}
			}
			count++;
		}
		// ----------------
		// System.out.println(article.getNamePositionMap());
		// System.out.println(article.getNamePositionRuleMap());
		// ----------------

		ArrayList<String> arrayList = new ArrayList<>();
		for (String value : article.getNamePositionMap().values()) {
			arrayList.add(value);
		}
		ArticleDao.updateArrayColumnContent(article.getId(),
				GlobalVariable.allPosPersonColumnName, arrayList);
		convertJson();
	}

	private void convertJson() {
		// TODO Auto-generated method stub
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		// System.out.println("jsonObject: " + jsonObject);
		int count = 0;
		for (Integer key2 : article.getNamePositionRuleMap().keySet()) {
			for (Integer key : article.getNamePositionMap().keySet()) {
				if (key2.toString().equals(key.toString())) {
					Position position = new Position();
					position.setName(article.getNamePositionMap().get(key));
					position.setPosition(key);
					position.setRuleExample(article.getNamePositionRuleMap()
							.get(key));
					// System.out.println(position.getName());
					// System.out.println(position.getPosition());
					// System.out.println(position.getRuleExample());
					JSONObject jsonObjectTemp = new JSONObject();
					jsonObjectTemp.put("name", position.getName());
					jsonObjectTemp.put("position", position.getPosition());
					jsonObjectTemp.put("example", position.getRuleExample());
					// jsonObject.put(String.valueOf(count), jsonObjectTemp);
					jsonArray.put(jsonObjectTemp);
					// jsonObject.put(key.toString(), "123");
					// JSONObject jsonObject1 = new JSONObject(position);
					// System.out.println("jsonObject1: " + jsonObject1);
					count = count + 1;
				}
			}
		}
		jsonObject.put("key", jsonArray);
		// System.out.println("jsonObject: " + jsonObject.toString());
		ArticleDao.updateStringColumnContent(article.getId(), "jsonAllPos",
				jsonObject.toString());
		JSONArray array = jsonObject.getJSONArray("key");
		for (int n = 0; n < array.length(); n++) {
			JSONObject object = array.getJSONObject(n);
			// System.out.println("object: "+object);
		}
	}

	static int StringMatchingAlgo(String article, String target) {
		// String t = "ababcabc";
		// String p = "abc";
		int time = 0;// 次數

		try {
			for (int i = 0; i < article.length(); i++) {
				// System.out.println("T的第"+i+"個字元開始為P");
				if (i + target.length() > article.length()) { // P不會超出T右端
					break;
				} else if (article.substring(i, i + target.length()).equals(
						target)) {
					// System.out.println(i+":"+p.length());
					time = time + 1;
				}
			}
			return time;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;

	}

	public void compareRule() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Rule personRule = new PersonRule();
		Rule personDaDaRule = new PersonDaDaRule();
		
		String allPosPersonResult = ArticleDao.getStringColumnContent(
				article.getId(), GlobalVariable.allPosPersonColumnName);
		try {
//			personRule.nameEntityRecognize(article);
			// System.out.println("before name chain: "
			// + article.getNamePositionMap());
			personDaDaRule.nameEntityRecognize(article);
			

			if (allPosPersonResult != null && !allPosPersonResult.equals("")) {

			} else {
				// ArticleDao.updateArrayListColumnContent(article.getId(),
				// GlobalVariable.allPosPersonColumnName,
				// personRule.nameEntityRecognize(article));

				// System.out.println("before name chain: "
				// + article.getNamePositionMap());
				// System.out.println("map: " +
				// article.getNamePositionRuleMap());

				// personRule.nameEntityRecognize(article);
				// System.out.println(personRule.nameEntityRecognize(article));
				// printMap(article.getNamePositionMap());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
