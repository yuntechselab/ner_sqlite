package builder;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import ner_sqlite.Ckip;
import entity.Article;
import entity.Position;
import entityDao.ArticleDao;

public abstract class NerBuilder {
	protected Article article;

	public Article getArticle() {
		return article;
	}

	public void createNewArticle() {
		article = new Article();
	}

	public abstract void keepPos();

	public abstract void compareRule() throws CloneNotSupportedException;

	public abstract void compareNameChain();

	public void importArticle(int id) {
		// TODO Auto-generated method stub
		article.setId(id);
		article.setContent(ArticleDao.getStringColumnContent(id, "article"));
		article.setCkipResult(ArticleDao.getStringColumnContent(id, "ckip"));
		// article.setAllPosPersonResult(ArticleDao.getArrayListColumnContent(id,
		// "questionPersonAllPos"));
		// article.setContent(ArticleDao.getStringColumnContent(id,
		// "questionPersonSomePos"));
		// article.setContent(ArticleDao.getStringColumnContent(id,
		// "questionPersonAllPosSomeRule"));

	}

	public void ckipMethod() {
		// TODO Auto-generated method stub

		try {
			if (article.getCkipResult() != null
					&& !article.getCkipResult().equals("")) {
				// System.out.println("yes ckip");
			} else {
				// System.out.println("no ckip");
				article.setCkipResult(Ckip.getCkipResult(article.getContent()));
				ArticleDao.updateStringColumnContent(article.getId(), "ckip",
						article.getCkipResult());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void segement() {
		// TODO Auto-generated method stub
		String rowToken = "\n----------------------------------------------------------------------------------------------------------------------------------\n";
		String rowToken2 = "----------------------------------------------------------------------------------------------------------------------------------";
		String splitHeadToken = "(";
		String splitTailToken = ")";
//		System.out.println(article.getCkipResult());
		for (String s : article.getCkipResult().split("　")) {
			String term = "";
			String pos = "";
			s = s.replace(rowToken2, "");
			String checkToken = s.substring(0, s.length());
//			System.out.println("checkToken: "+checkToken);

			if (checkToken.indexOf(splitHeadToken) != -1) {
				term = s.substring(0, s.indexOf(splitHeadToken)).trim();
				pos = s.substring(s.indexOf(splitHeadToken), s.length());
				pos = pos.replace(splitHeadToken, "");
				pos = pos.replace(splitTailToken, "");
//				System.out.println("term: "+term);
//				System.out.println("pos: "+pos);

				article.getPosList().add(pos);
				article.getTermList().add(term);
				checkToken = "";
			}
		}
//		System.out.println(article.getPosList());
//		System.out.println(article.getTermList());
		convertJson();
	}
	private void convertJson() {
		// TODO Auto-generated method stub
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		// System.out.println("jsonObject: " + jsonObject);
		
		for (int posOrder = 0; posOrder<article.getPosList().size();posOrder++) {
					
					// System.out.println(position.getName());
					// System.out.println(position.getPosition());
					// System.out.println(position.getRuleExample());
					JSONObject jsonObjectTemp = new JSONObject();
					jsonObjectTemp.put("position", posOrder);
					jsonObjectTemp.put("pos", article.getPosList().get(posOrder));
					jsonObjectTemp.put("term", article.getTermList().get(posOrder));					
					// jsonObject.put(String.valueOf(count), jsonObjectTemp);
					jsonArray.put(jsonObjectTemp);
					// jsonObject.put(key.toString(), "123");
					// JSONObject jsonObject1 = new JSONObject(position);
					// System.out.println("jsonObject1: " + jsonObject1);				
		}
		jsonObject.put(article.getId().toString(), jsonArray);
//		 System.out.println("jsonObject: " + jsonObject.toString());
		article.setJsonObject(jsonObject);
//		ArticleDao.updateStringColumnContent(article.getId(), "jsonAllPos",
//				jsonObject.toString());
		
		JSONArray array = jsonObject.getJSONArray(article.getId().toString());
		for (int n = 0; n < array.length(); n++) {
			JSONObject object = array.getJSONObject(n);
//			 System.out.println("object: "+object);
		}
	}
}
